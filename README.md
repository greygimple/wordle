# Wordle Bot

## About The Project

Python script that filters the possible words based on game logic and rules

## Prerequisites

- Python 3

## Download

```bash
git clone https://gitlab.com/greygimple/wordle.git

cd wordle
```

## Contact

Want to contribute and help add more features? Fork the project and open a pull request!

If you wish to contact me, you can reach out at greygimple@gmail.com
